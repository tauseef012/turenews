package net.mightyegg.turenews;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import net.mightyegg.turenews.SimpleGestureFilter.SimpleGestureListener;
public class MainActivity extends AppCompatActivity implements SimpleGestureListener{
    private SimpleGestureFilter detector;
    android.support.v7.widget.CardView cv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        detector = new SimpleGestureFilter(this,this);
        cv=(android.support.v7.widget.CardView) findViewById(R.id.cv);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT : str = "Swipe Right egg";
                //  Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                cv.animate().translationX(1000);
               new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i=new Intent(getApplicationContext(),Profile.class);
                      
                        startActivity(i);
                        finish();
                    }
                }, 800);

                cv.animate().translationX(500);
                break;
            case SimpleGestureFilter.SWIPE_LEFT :  str = "Swipe Left egg";
                // Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                break;
            case SimpleGestureFilter.SWIPE_DOWN :  str = "Swipe Down egg";
                // Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                break;
            case SimpleGestureFilter.SWIPE_UP :    str = "Swipe Up egg";
                //Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                break;

        }
        // Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {
        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }

    public void onBackPressed(){
        // do something here and don't write super.onBackPressed()

      // cv.animate().translationX(1000);
        //cv.animate().translationX(-1000);

        Animation anim = new TranslateAnimation(0, 1000, 0, 0);
        anim.setFillAfter(true);
        anim.setDuration(1000);
        cv.startAnimation(anim);

        cv.setVisibility(View.GONE);

        Animation anim1 = new TranslateAnimation(0, -1500, 0, 0);
        anim1.setFillAfter(true);
        anim1.setDuration(1000);
        cv.startAnimation(anim1);
        cv.setVisibility(View.VISIBLE);



 new Handler().postDelayed(new Runnable() {

     @Override
     public void run() {
               /*  Intent i = new Intent(getApplicationContext(), Profile.class);

                startActivity(i);
                finish();
                  */
        // cv.animate().translationX(90);
        // cv.setVisibility(View.INVISIBLE);
        // cv.animate().translationX(-900);
        // cv.setVisibility(View.VISIBLE);

     //    AnimationSet animationSet = new AnimationSet(true);
//         TranslateAnimation a = new TranslateAnimation(
//                 Animation.ABSOLUTE,200, Animation.ABSOLUTE,200,
//                 Animation.ABSOLUTE,200, Animation.ABSOLUTE,200);
//         TranslateAnimation a= new TranslateAnimation(0, 500, 0, 0);
//         a.setDuration(1000);
//         a.setFillAfter(true); //HERE
//         animationSet.addAnimation(a);
//         cv.startAnimation(animationSet);

        // ObjectAnimator transAnimation= ObjectAnimator.ofFloat(cv,View.ALPHA, 0f, 1000f);
         //transAnimation.setDuration(3000);//set duration
         //transAnimation.start();//start animation


// or like this
      //   Animation anim = AnimationUtils.loadAnimation(this, R.anim.animation_name);


         Animation anim3 = new TranslateAnimation(0,0, 0, 0);
         anim3.setFillAfter(true);
         anim3.setDuration(1000);
         cv.startAnimation(anim3);

     }
 }, 3000);


    }



}
