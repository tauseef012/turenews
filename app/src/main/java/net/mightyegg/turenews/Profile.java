package net.mightyegg.turenews;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.Toast;

import net.mightyegg.turenews.SimpleGestureFilter.SimpleGestureListener;

/**
 * Created by Mighty Egg on 12/28/2015.
 */
public class Profile extends Activity  implements SimpleGestureListener {
    private SimpleGestureFilter detector;
    LinearLayout llprofile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        detector = new SimpleGestureFilter(this,this);
        llprofile=(LinearLayout)findViewById(R.id.llprofile);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me){
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }
    @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT : str = "Swipe Right egg";
                //  Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                llprofile.animate().translationX(1000);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i=new Intent(getApplicationContext(),MainActivity.class);

                        startActivity(i);
                        finish();
                    }
                }, 800);
                break;
            case SimpleGestureFilter.SWIPE_LEFT :  str = "Swipe Left egg";
                // Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();

                break;
            case SimpleGestureFilter.SWIPE_DOWN :  str = "Swipe Down egg";
                // Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                break;
            case SimpleGestureFilter.SWIPE_UP :    str = "Swipe Up egg";
                //Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
                break;

        }
        // Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {
        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }


    public void onBackPressed(){
        // do something here and don't write super.onBackPressed()
        llprofile.animate().translationX(1000);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);

                startActivity(i);
                finish();
            }
        }, 800);

    }


}
